@interface GBMHangoutIntent : NSObject
@property(assign) void (^intentActionCallback)(GBMHangoutIntent*);
-(id)initWithContact:(id)person phoneNumber:(id)number intentType:(NSUInteger)type;
-(id)initWithDialedNumber:(NSString*)number regionCode:(NSString*)region;
-(id)contact;
@end
@interface GBMUserClient
-(NSString*)regionCode;
@end
@interface GBAPhoneCallNavigationContext
+(void)navigateToPhoneCallWithUserClient:(GBMUserClient*)client intent:(GBMHangoutIntent*)intent;
@end
@interface GBAConversationDetailsViewController
-(void)navigateToContactForPerson:(id)person;
@end

static Class cGBMHangoutIntent;

%hook GBMAppLaunchUrlHandler
-(id)appLaunchOptionsForInternalUrl:(NSURL*)URL isSecure:(BOOL)secure userClient:(GBMUserClient*)client {
  if([URL.host isEqualToString:@"call"]){
    GBMHangoutIntent* intent=[[cGBMHangoutIntent alloc]
     initWithDialedNumber:URL.query regionCode:client.regionCode];
    static Class cls=NULL;
    if(!cls){cls=objc_getClass("GBAPhoneCallNavigationContext");}
    [cls navigateToPhoneCallWithUserClient:client intent:intent];
    [intent release];
    return nil;
  }
  return %orig;
}
%end

%hook GBAConversationPeopleViewController
-(NSMutableArray*)intentsForPhoneEntity:(id)contact {
  NSMutableArray* intents=%orig;
  GBMHangoutIntent* intent=[[cGBMHangoutIntent alloc]
   initWithContact:contact phoneNumber:nil intentType:9];
  objc_setAssociatedObject(intent,&cGBMHangoutIntent,
   @"View contact",OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  intent.intentActionCallback=^(GBMHangoutIntent* intent){
   [self navigateToContactForPerson:intent.contact];};
  [intents addObject:intent];
  [intent release];
  return intents;
}
%end

%hook GBAIntentSelectorViewController
-(NSString*)titleForIntent:(GBMHangoutIntent*)intent {
  return objc_getAssociatedObject(intent,&cGBMHangoutIntent)?:%orig;
}
%end

%ctor {
  cGBMHangoutIntent=objc_getClass("GBMHangoutIntent");
  %init;
}
